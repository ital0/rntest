import firestore from '@react-native-firebase/firestore';

export const fetchQuestions = () => {
  return firestore()
    .collection('questions')
    .get()
    .then(snapshot => snapshot.docs);
};
