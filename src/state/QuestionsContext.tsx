import * as React from 'react';
import { QuestionProps } from '../components/quiz/types';
import { FirebaseFirestoreTypes } from '@react-native-firebase/firestore';

type QuestionsContextType = {
  questions: Array<QuestionProps>;
  setQuestions: (questions: FirebaseFirestoreTypes.DocumentData[]) => void;
};

export const QuestionsContext = React.createContext<QuestionsContextType>({
  questions: [],
  setQuestions: (questions) => {},
});

export const useQuestionsContext = () => React.useContext(QuestionsContext);
