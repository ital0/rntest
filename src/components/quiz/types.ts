type QuestionAnswerProps = {
  word: string;
  isCorrect: boolean;
};

type QuestionProps = {
  sentence: string;
  sentenceTranslate: string;
  word: string;
  type: string;
  answers: Array<QuestionAnswerProps>;
};

export type { QuestionProps, QuestionAnswerProps };
