import * as React from 'react';
import { View, Text } from 'react-native';
import { QuestionProps } from './types';
import { QuizItemStyles } from './styles';
import Button from '../common/button/Button';

interface QuizItemProps {
  question: QuestionProps;
  parentSucessCallback: any;
}

const InterpolateWord = (text: string, values: any, remove?: boolean) => {
  const pattern = /([$word]+)/g;
  const matches = text.match(pattern);
  const parts = text.split(pattern);

  const replaceWord = (word: string) => word ? word.replace(/\D/g, '_') : '';

  if (!matches) {
    return text;
  }

  return parts.map((part: string, index: number) => {
    if (!matches.includes(part)) {
      return part;
    }
    return (
      <Text
        key={part + index}
        style={{ fontWeight: 'bold', textDecorationLine: 'underline' }}
      >
        {matches.includes(part)
          ? remove
            ? replaceWord(values[part])
            : values[part]
          : part}
      </Text>
    );
  });
};

const QuizItem: React.FC<QuizItemProps> = props => {
  const { question, parentSucessCallback } = props;
  const [selectedOption, setSelected] = React.useState<QuestionProps|null>(null);
  const [isAnswerCorrect, setAnswerCorrect] = React.useState(false);
  const [isAnswerWrong, setAnswerWrong] = React.useState(false);

  const originalWord = question.word;
  const translatedWord = question.answers.find(q => q.isCorrect);

  const renderOptions = () => {
    return question.answers.map(a => {
      const isSelected = !!selectedOption && selectedOption.word === a.word;
      return (
        <Button
          title={a.word}
          action={() => {
            setAnswerWrong(false);
            setAnswerCorrect(false);
            if (isSelected) {
              return setSelected(null);
            }
            setSelected(a);
          }}
          style={[
            QuizItemStyles.optionButton,
            isSelected ? QuizItemStyles.optionButtonSelected : null,
          ]}
          textStyle={QuizItemStyles.optionButtonText}
        />
      );
    });
  };
  const renderButtonText = () => {
    if (
      !selectedOption ||
      (selectedOption && (isAnswerCorrect || isAnswerWrong))
    ) {
      return 'Continue';
    }
    return 'Check answer';
  };
  const renderCheckText = () => {
    return (
      <Text style={QuizItemStyles.resultText}>
        {isAnswerCorrect ? 'Great Job!' : `Answer: ${translatedWord.word}`}
      </Text>
    );
  };

  const goToNext = () => {
    setSelected(null);
    setAnswerCorrect(false);
    setAnswerWrong(false);
    parentSucessCallback();
  };

  const checkAnswer = () => {
    if (isAnswerCorrect || isAnswerWrong) {
      return goToNext();
    }
    if (selectedOption.word === translatedWord.word) {
      setAnswerWrong(false);
      return setAnswerCorrect(true);
    }
    setAnswerWrong(true);
    setAnswerCorrect(false);
  };
  const setCheckContainer = () => {
    if (isAnswerWrong) {
      return { backgroundColor: '#ff8e89', paddingTop: 20 };
    }
    return { backgroundColor: '#00dee9', paddingTop: 20 };
  };
  return (
    <View style={QuizItemStyles.container}>
      <View style={QuizItemStyles.contentContainer}>
        <Text style={QuizItemStyles.questionText}>
          {InterpolateWord(question.sentence, { $word: originalWord })}
        </Text>

        <Text
          style={[
            QuizItemStyles.questionText,
            { paddingTop: 25, fontSize: 20 },
          ]}
        >
          {InterpolateWord(
            question.sentenceTranslate,
            {
              $word: selectedOption ? selectedOption.word : translatedWord.word,
            },
            !selectedOption,
          )}
        </Text>

        <View style={QuizItemStyles.optionsContainer}>{renderOptions()}</View>
      </View>

      <View
        style={[
          QuizItemStyles.checkContainer,
          isAnswerCorrect || isAnswerWrong ? setCheckContainer() : null,
        ]}
      >
        {isAnswerCorrect || isAnswerWrong ? renderCheckText() : null}
        <View style={{ alignItems: 'center' }}>
          <Button
            title={renderButtonText()}
            action={() => checkAnswer()}
            style={{ width: '90%', marginTop: 20 }}
            isDisabled={!selectedOption}
          />
        </View>
      </View>
    </View>
  );
};

export default QuizItem;
