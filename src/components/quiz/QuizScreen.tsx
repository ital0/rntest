import * as React from 'react';
import { Text, View } from 'react-native';
import { NativeStackNavigationProp } from '@react-navigation/native-stack';

import Header from '../common/header/Header';
import Container from '../common/container/Container';
import { QuestionProps } from './types';
import { useQuestionsContext } from '../../state/QuestionsContext';
import QuizItem from './QuizItem';

interface QuizScreenProps {
  navigation: NativeStackNavigationProp<any, any>;
  questions: Array<QuestionProps>;
}

const QuizScreen: React.FC<QuizScreenProps> = props => {
  const { questions } = useQuestionsContext();
  const [index, setIndex] = React.useState(0);

  const renderQuizItem = () => (
    <QuizItem
      question={questions[index]}
      parentSucessCallback={() => {
        if (index >= (questions.length - 1)) {
          return;
        }
        setIndex(index + 1);
      }}
    />
  );

  return (
    <Container>
      <Header backAction={() => props.navigation.goBack()} />
      <View
        style={{
          marginTop: 30,
          backgroundColor: '#3c6c82',
          borderTopStartRadius: 15,
          borderTopEndRadius: 15,
          height: '100%',
          alignItems: 'center',
          // justifyContent: 'center',
          paddingVertical: 30,
        }}
      >
        <Text
          style={{
            color: '#FFFFFF',
            fontSize: 14,
          }}
        >
          Fill in the missing word
        </Text>

        {renderQuizItem()}
      </View>
    </Container>
  );
};

export default QuizScreen;
