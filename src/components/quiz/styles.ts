import { StyleSheet } from 'react-native';

const QuizItemStyles = StyleSheet.create({
  container: {
    paddingVertical: 20,
    flex: 1,
  },
  contentContainer: {
    flex: 2,
    alignItems: 'center',
  },
  optionsContainer: {
    paddingTop: 40,
    justifyContent: 'center',
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  optionButton: {
    backgroundColor: '#FFFFFF',
    marginHorizontal: 5,
    marginVertical: 5,
  },
  optionButtonSelected: {
    backgroundColor: '#11e4ea',
  },
  optionButtonText: {
    color: '#6392a6',
    fontWeight: '700',
    fontSize: 14,
  },
  questionText: {
    fontSize: 25,
    color: '#FFF',
  },
  checkContainer: {
    flex: 1,
    borderRadius: 30,
  },
  resultText: {
    fontWeight: 'bold',
    fontSize: 18,
    paddingLeft: 25,
    color: '#FFFFFF',
  },
});

const QuizScreenStyles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export { QuizItemStyles, QuizScreenStyles };
