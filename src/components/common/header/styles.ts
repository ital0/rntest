import { StyleSheet } from 'react-native';

const HeaderStyles = StyleSheet.create({
  container: {
    backgroundColor: '#76dafe',
    height: 60,
    width: '100%',
  },
});

export default HeaderStyles;
