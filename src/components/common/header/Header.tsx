import * as React from 'react';
import { View } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

import HeaderStyles from './styles';

interface HeaderProps {
  backAction: any;
}

const Header: React.FC<HeaderProps> = props => {
  return (
    <View style={HeaderStyles.container}>
      <Icon.Button
        name="chevron-left"
        onPress={props.backAction}
        backgroundColor="#76dafe"
      />
      {props.children}
    </View>
  );
};

export default Header;
