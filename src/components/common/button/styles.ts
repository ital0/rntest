import { StyleSheet } from 'react-native';

const ButtonStyles = StyleSheet.create({
  container: {
    minWidth: 120,
    height: 50,
    borderRadius: 30,
    backgroundColor: '#6392a6',
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.4,
    shadowRadius: 6,
    elevation: 10,
  },
  text: {
    fontSize: 25,
    fontWeight: 'bold',
    color: '#FFFFFF',
  },
  enabled: {
    backgroundColor: '#11e4ea',
  },
});

export default ButtonStyles;
