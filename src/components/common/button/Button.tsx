import * as React from 'react';
import { Text, TouchableOpacity } from 'react-native';

import ButtonStyles from './styles';

interface ButtonProps {
  title: string;
  isDisabled?: boolean;
  action: any;
  style?: any;
  textStyle?: any;
}

const Button: React.FC<ButtonProps> = props => {
  const {
    title,
    isDisabled,
    action,
    style: customStyle,
    textStyle: customTextStyle,
  } = props;
  return (
    <TouchableOpacity
      onPress={action}
      style={[
        ButtonStyles.container,
        !isDisabled ? ButtonStyles.enabled : null,
        customStyle,
      ]}
      disabled={!!isDisabled}
    >
      <Text style={[ButtonStyles.text, customTextStyle]}>
        {title}
      </Text>
    </TouchableOpacity>
  );
};

export default Button;
