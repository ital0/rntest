import * as React from 'react';
import { View } from 'react-native';

import ContainerStyles from './styles';

interface ContainerProps {
  style?: any;
}

const Container: React.FC<ContainerProps> = props => {
  return (
    <View style={[ContainerStyles.container, props.style]}>
      {props.children}
    </View>
  );
};

export default Container;
