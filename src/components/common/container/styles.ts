import { StyleSheet } from 'react-native';

const ContainerStyles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#76dafe',
  },
});

export default ContainerStyles;
