import * as React from 'react';
import { Text } from 'react-native';
import { NativeStackNavigationProp } from '@react-navigation/native-stack';

import HomeScreenStyles from './styles';
import Button from '../common/button/Button';
import Container from '../common/container/Container';
import { useQuestionsContext } from '../../state/QuestionsContext';
import { fetchQuestions } from '../../integrations/firebase-firestore';

interface HomeScreenProps {
  navigation: NativeStackNavigationProp<any, any>;
}

const HomeScreen: React.FC<HomeScreenProps> = props => {
  const { navigation } = props;
  const [isQuizAvailable, setQuizAvailable] = React.useState(false);
  const { setQuestions } = useQuestionsContext();

  React.useEffect(async () => {
    const getQuestions = await fetchQuestions();
    const questions = getQuestions.map(question => question.data());
    setQuestions(questions);
    setQuizAvailable(true);
  }, []);

  const renderWaitText = () => {
    const textStyle = { marginTop: 50, color: '#FFF', fontSize: 12 };
    return (
      <Text style={textStyle}>
        Wait a few second while we are fetching data...
      </Text>
    );
  };

  return (
    <Container style={HomeScreenStyles.container}>
      <Button
        title="Start Quiz"
        action={() => navigation.navigate('Quiz')}
        isDisabled={!isQuizAvailable}
        style={{ width: '60%' }}
      />
      {!isQuizAvailable ? renderWaitText() : null}
    </Container>
  );
};

export default HomeScreen;
