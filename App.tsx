import React from 'react';
import { SafeAreaView } from 'react-native';

import Router from './src/Router';
import { QuestionsContext } from './src/state/QuestionsContext';
import { QuestionProps } from './src/components/quiz/types';

const App = () => {
  const [ questions, setQuestions ] = React.useState<Array<QuestionProps>>(  []);
  return (
    <QuestionsContext.Provider value={{ questions, setQuestions }}>
      <SafeAreaView style={{ flex: 1 }}>
        <Router />
      </SafeAreaView>
    </QuestionsContext.Provider>
  );
};

export default App;
